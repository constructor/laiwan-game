package com.java80.littlegame.common.base;

import java.util.Properties;

import com.java80.littlegame.common.utils.LoadPropertiesFileUtil;

public class BaseConfig {
	private static int threadSize;

	static {
		init();
	}

	public static void init() {
		Properties p = LoadPropertiesFileUtil.loadProperties("../config/cfg.properties");
		threadSize = Integer.parseInt(p.getProperty("base.threadPool.worksize"));
	}

	public static int getThreadSize() {
		return threadSize;
	}

}
