package com.java80.littlegame.common.db.dao.base;

import java.util.List;

public interface BaseSysDaoInterface<T> extends BaseDaoInterface<T> {
	public List<T> getAll() throws Exception;
}
