package com.java80.littlegame.common.db.dao.dbImpl;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import com.java80.littlegame.common.db.dao.URoomInfoDao;
import com.java80.littlegame.common.db.dao.base.BaseDao;
import com.java80.littlegame.common.db.entity.URoomInfo;

public class URoomInfoDaoDBImpl extends BaseDao implements URoomInfoDao {

	@Override
	public void delete(long id) throws SQLException {
		String sql = "delete from u_room_info where roomId=?";
		super.delete(sql, Arrays.asList(id));
	}

	@Override
	public void insert(URoomInfo t) throws SQLException {
		String sql = "insert into u_room_info values(?,?,?,?,?,now())";
		super.insert(sql, Arrays.asList(t.getRoomId(), t.getGameId(), t.getPassword(), t.getStatus(), t.getUserId()));
	}

	@Override
	public void update(URoomInfo t) throws SQLException {
		String sql = "update u_room_info set status=? where roomId=?";
		super.update(sql, Arrays.asList(t.getStatus(), t.getRoomId()));
	}

	@Override
	public void deletByUserId(long userId) throws SQLException {
		String sql = "delete from u_room_info where userId=?";
		super.delete(sql, Arrays.asList(userId));

	}

	@Override
	public List<URoomInfo> getAllByUserId(long userId) throws Exception {
		String sql = "select * from u_room_info where userId=?";
		return find(sql, Arrays.asList(userId), URoomInfo.class);
	}

	@Override
	public URoomInfo get(long id) throws Exception {
		String sql = "select * from u_room_info where roomId=?";
		return super.get(sql, Arrays.asList(id), URoomInfo.class);
	}

}
