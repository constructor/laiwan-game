package com.java80.littlegame.common.base;

public interface BaseMessageCustomer {
	void consumeMsgByQueue(String queueName);

	void consumeMsgByExchange(String exchangeName);
}
