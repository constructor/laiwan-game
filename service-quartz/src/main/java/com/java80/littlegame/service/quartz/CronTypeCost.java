package com.java80.littlegame.service.quartz;

public class CronTypeCost {
	public static final String PREFIX_USER_CRON_JOG = "cron_user_";
	public static final String PREFIX_SYS_LOOP_CRON_JOG = "cron_sys__";
	public static final String PREFIX_SYS_CRON_JOG = "cron_sys__";

	/** 建筑升级(新建) **/
	public static final int TYPE_BUILDING_UPGRADE = 200;
	/** 温泉，恢复HP **/
	public static final int TYPE_RECOVER_HP = 201;
	/** 训练英雄 **/
	public static final int TYPE_TRAIN_HERO = 202;
	/** 生产道具 **/
	public static final int TYPE_PRODUCE_PROP = 203;
	/**  **/
	public static final int TYPE_FIGHT_UPGRADE = 301;
	public static final int TYPE_FIGHT_COUNTRY = 311;
	public static final int TYPE_FIGHT_CATDOJO = 321;

	public static final int TYPE_REFRESH_UPGRADE = 302;

}
