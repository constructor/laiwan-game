package com.java80.littlegame.service.quartz;

import java.util.Set;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.Member;
import com.hazelcast.core.MemberAttributeEvent;
import com.hazelcast.core.MembershipEvent;
import com.hazelcast.core.MembershipListener;

import io.vertx.spi.cluster.hazelcast.impl.HazelcastClusterNodeInfo;

public class HTes {
	public static void main(String[] args) {
		HazelcastClusterNodeInfo c = new HazelcastClusterNodeInfo();
		HazelcastInstance instance = Hazelcast.newHazelcastInstance();
		Set<Member> members = instance.getCluster().getMembers();
		instance.getCluster().addMembershipListener(new MembershipListener() {

			@Override
			public void memberRemoved(MembershipEvent membershipEvent) {
				// TODO Auto-generated method stub
				System.out.println("member remo");
				String stringAttribute = membershipEvent.getMember().getStringAttribute("sname");
				System.out.println(stringAttribute);
			}

			@Override
			public void memberAttributeChanged(MemberAttributeEvent memberAttributeEvent) {
				// TODO Auto-generated method stub

			}

			@Override
			public void memberAdded(MembershipEvent membershipEvent) {
				// TODO Auto-generated method stub
				System.out.println("meber add");
				String stringAttribute = membershipEvent.getMember().getStringAttribute("sname");
				System.out.println(stringAttribute);

			}
		});
		for (Member member : members) {
			System.out.println(member.getStringAttribute("serverName"));
		}
	}
}
